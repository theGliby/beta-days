package com.legacy.betadays;

import net.minecraft.block.CakeBlock;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.entity.monster.PhantomEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.AxeItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.Food;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.FluidTags;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingExperienceDropEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickBlock;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickItem;
import net.minecraftforge.event.entity.player.PlayerPickupXpEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@SuppressWarnings("deprecation")
public class BetaPlayerEvents
{
	public static final AttributeModifier oldSpeed = new AttributeModifier("Beta Speed Modifier", 999999D, Operation.ADDITION);

	@SubscribeEvent
	public void PlayerUpdateEvent(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof PlayerEntity)
		{
			PlayerEntity player = (PlayerEntity) event.getEntityLiving();

			if (BetaDaysConfig.disableCombatCooldown)
			{
				if (!player.getAttributes().getAttributeInstance(SharedMonsterAttributes.ATTACK_SPEED).hasModifier(oldSpeed))
				{
					player.getAttributes().getAttributeInstance(SharedMonsterAttributes.ATTACK_SPEED).applyModifier(oldSpeed);
				}
			}
			else
			{
				if (player.getAttributes().getAttributeInstance(SharedMonsterAttributes.ATTACK_SPEED).hasModifier(oldSpeed))
				{
					player.getAttributes().getAttributeInstance(SharedMonsterAttributes.ATTACK_SPEED).removeModifier(oldSpeed);
				}
			}

			if (BetaDaysConfig.hungerDisabled)
			{
				if (player.getFoodStats().getFoodLevel() != 7)
				{
					player.getFoodStats().setFoodLevel(7);
				}
			}

			if (BetaDaysConfig.disableSprinting && !player.isCreative())
			{
				if (player.getAir() < player.getMaxAir() && !player.areEyesInFluid(FluidTags.WATER))
				{
					player.setAir(player.getMaxAir());
				}

				player.setSprinting(false);
			}
		}
	}

	@SubscribeEvent
	public void onPlayerEatCake(RightClickBlock event)
	{
		if (event.getWorld().getBlockState(event.getPos()).getBlock() instanceof CakeBlock)
		{
			if (BetaDaysConfig.hungerDisabled)
			{
				event.getPlayer().heal(2.0F);
			}
		}
	}

	@SubscribeEvent
	public void onPlayerRightClick(RightClickItem event)
	{
		ItemStack item = event.getItemStack();
		PlayerEntity player = (PlayerEntity) event.getEntityLiving();

		if (item.getItem().isFood())
		{
			Food food = item.getItem().getFood();
			if (player.getHealth() >= player.getMaxHealth())
			{
				event.setCanceled(true);
			}
			else if (BetaDaysConfig.disableFoodStacking && BetaDaysConfig.hungerDisabled)
			{
				item.getItem().onItemUseFinish(item, player.world, player);
				player.heal(food.getHealing());

				event.setCanceled(true);
			}
		}
		else if (item.getItem() instanceof BowItem)
		{
			BowItem bow = (BowItem) item.getItem();

			if (BetaDaysConfig.originalBow)
			{
				bow.onPlayerStoppedUsing(item, player.world, player, 200);
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public void onEntityXPDrop(LivingExperienceDropEvent event)
	{
		if (BetaDaysConfig.disableExperienceDrop)
			event.setCanceled(true);
	}

	@SubscribeEvent
	public void onEntityXPPickup(PlayerPickupXpEvent event)
	{
		if (BetaDaysConfig.disableExperienceDrop)
		{
			event.getOrb().remove();
			event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public void onEntityDamaged(LivingDamageEvent event)
	{
		if (BetaDaysConfig.disableCombatCooldown && event.getSource().getImmediateSource() instanceof LivingEntity && ((LivingEntity) event.getSource().getImmediateSource()).getHeldItemMainhand().getItem() instanceof AxeItem)
			event.setAmount(event.getAmount() - 5);
	}

	@SubscribeEvent
	public void onLivingSpawn(LivingSpawnEvent event)
	{
		if (event.getEntityLiving() instanceof PhantomEntity && BetaDaysConfig.disablePhantomSpawns)
		{
			event.getEntityLiving().remove();
		}
	}

	@SubscribeEvent
	public void onItemFinishUse(LivingEntityUseItemEvent.Finish event)
	{
		if (!BetaDaysConfig.disableFoodStacking && BetaDaysConfig.hungerDisabled && event.getEntityLiving() instanceof PlayerEntity)
		{
			if (event.getItem().isFood())
			{
				Food food = event.getItem().getItem().getFood();

				event.getItem().getItem().onItemUseFinish(event.getItem(), event.getEntityLiving().world, event.getEntityLiving());
				event.getEntityLiving().heal(food.getHealing());
			}
		}
	}
}