package com.legacy.betadays.client.gui;

import com.legacy.betadays.BetaDays;
import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.gui.AccessibilityScreen;
import net.minecraft.client.gui.screen.*;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.ImageButton;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SharedConstants;
import net.minecraft.util.Util;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.client.gui.GuiModList;

public class ClassicMainMenuScreen extends MainMenuScreen
{
	private static final ResourceLocation MINECRAFT_TITLE_TEXTURES = new ResourceLocation("textures/gui/title/minecraft.png");
	private static final ResourceLocation ACCESSIBILITY_TEXTURES = new ResourceLocation("textures/gui/accessibility.png");
	private static final ResourceLocation FORGE_BUTTON = BetaDays.locate("textures/gui/forge.png");
	private final boolean showFadeInAnimation;
	private String splashText;
	private int widthCopyright;
	private int widthCopyrightRest;
	private long firstRenderTime;

	public ClassicMainMenuScreen()
	{
		this(false);
	}

	public ClassicMainMenuScreen(boolean fadeIn)
	{
		this.showFadeInAnimation = fadeIn;
	}

	@Override
	protected void init()
	{
		if (this.splashText == null)
		{
			this.splashText = this.minecraft.getSplashes().getSplashText();
		}

		this.widthCopyright = this.font.getStringWidth("Copyright Mojang AB. Do not distribute.");
		this.widthCopyrightRest = this.width - this.widthCopyright - 2;
		int j = this.height / 4 + 48;
		this.addSingleplayerMultiplayerButtons(j, 24);

		this.addButton(new ImageButton(this.width - 25, 3 * 2, 20, 20, 0, 0, 20, FORGE_BUTTON, 32, 64, (onPress) ->
		{
			this.minecraft.displayGuiScreen(new GuiModList(this));
		}));

		this.addButton(new ImageButton(this.width - 25, 3 * 18, 20, 20, 0, 106, 20, Button.WIDGETS_LOCATION, 256, 256, (onPress) ->
		{
			this.minecraft.displayGuiScreen(new LanguageScreen(this, this.minecraft.gameSettings, this.minecraft.getLanguageManager()));
		}, I18n.format("narrator.button.language")));
		this.addButton(new Button(this.width / 2 - 100, j + (24 * 2), 200, 20, I18n.format("menu.options"), (onPress) ->
		{
			this.minecraft.displayGuiScreen(new OptionsScreen(this, this.minecraft.gameSettings));
		}));
		this.addButton(new Button(this.width / 2 - 100, j + (24 * 3), 200, 20, I18n.format("beta.menu.modsTextures"), (onPress) ->
		{
			this.minecraft.displayGuiScreen(new ResourcePacksScreen(this));
		}));
		this.addButton(new ImageButton(this.width - 25, 3 * 10, 20, 20, 0, 0, 20, ACCESSIBILITY_TEXTURES, 32, 64, (onPress) ->
		{
			this.minecraft.displayGuiScreen(new AccessibilityScreen(this, this.minecraft.gameSettings));
		}, I18n.format("narrator.button.accessibility")));

		this.minecraft.setConnectedToRealms(false);
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks)
	{
		if (this.firstRenderTime == 0L && this.showFadeInAnimation)
		{
			this.firstRenderTime = Util.milliTime();
		}

		float f = this.showFadeInAnimation ? (float) (Util.milliTime() - this.firstRenderTime) / 1000.0F : 1.0F;
		fill(0, 0, this.width, this.height, -1);
		int j = this.width / 2 - 137;

		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		GlStateManager.color4f(1.0F, 1.0F, 1.0F, this.showFadeInAnimation ? (float) MathHelper.ceil(MathHelper.clamp(f, 0.0F, 1.0F)) : 1.0F);
		blit(0, 0, this.width, this.height, 0.0F, 0.0F, 16, 128, 16, 128);
		float f1 = this.showFadeInAnimation ? MathHelper.clamp(f - 1.0F, 0.0F, 1.0F) : 1.0F;
		int l = MathHelper.ceil(f1 * 255.0F) << 24;

		if ((l & -67108864) != 0)
		{
			this.renderBackground(0);
			this.minecraft.getTextureManager().bindTexture(MINECRAFT_TITLE_TEXTURES);
			GlStateManager.color4f(1.0F, 1.0F, 1.0F, f1);
			this.blit(j + 0, 30, 0, 0, 155, 44);
			this.blit(j + 155, 30, 0, 45, 155, 44);

			if (this.splashText != null)
			{
				GlStateManager.pushMatrix();
				GlStateManager.translatef((float) (this.width / 2 + 90), 70.0F, 0.0F);
				GlStateManager.rotatef(-20.0F, 0.0F, 0.0F, 1.0F);
				float f2 = 1.8F - MathHelper.abs(MathHelper.sin((float) (Util.milliTime() % 1000L) / 1000.0F * ((float) Math.PI * 2F)) * 0.1F);
				f2 = f2 * 100.0F / (float) (this.font.getStringWidth(this.splashText) + 32);
				GlStateManager.scalef(f2, f2, f2);
				this.drawCenteredString(this.font, this.splashText, 0, -8, 16776960 | l);
				GlStateManager.popMatrix();
			}

			String s = "Minecraft " + SharedConstants.getVersion().getName();
			s = s + ("release".equalsIgnoreCase(this.minecraft.getVersionType()) ? "" : "/" + this.minecraft.getVersionType());

			String releaseType = this.minecraft.getVersionType();
			this.drawString(this.font, TextFormatting.DARK_GRAY + "Minecraft " + releaseType.replace("r", "R") + " " + SharedConstants.getVersion().getName(), 2, 3 * 2, 16777215 | l);
			this.drawString(this.font, "Copyright Mojang AB. Do not distribute.", this.widthCopyrightRest, this.height - 10, 16777215 | l);

			for (Widget widget : this.buttons)
			{
				widget.setAlpha(f1);
			}
		}

		for (int r = 0; r < this.buttons.size(); ++r)
		{
			this.buttons.get(r).render(mouseX, mouseY, partialTicks);
		}

	}

	private void addSingleplayerMultiplayerButtons(int yIn, int rowHeightIn)
	{
		this.addButton(new Button(this.width / 2 - 100, yIn, 200, 20, I18n.format("menu.singleplayer"), (p_213089_1_) ->
		{
			this.minecraft.displayGuiScreen(new WorldSelectionScreen(this));
		}));
		this.addButton(new Button(this.width / 2 - 100, yIn + rowHeightIn * 1, 200, 20, I18n.format("menu.multiplayer"), (p_213086_1_) ->
		{
			this.minecraft.displayGuiScreen(new MultiplayerScreen(this));
		}));
	}
}
