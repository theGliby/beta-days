package com.legacy.betadays.client.gui;

import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.resources.I18n;

public class LoadingDimensionsScreen extends Screen
{
	public boolean nether;

	public boolean leaving;

	public LoadingDimensionsScreen(boolean isNether, boolean leaving)
	{
		super(NarratorChatListener.EMPTY);
		this.nether = isNether;
		this.leaving = leaving;
	}

	public boolean shouldCloseOnEsc()
	{
		return false;
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks)
	{
		this.renderDirtBackground(0);
		this.drawCenteredString(this.font, I18n.format("menu.generatingTerrain"), this.width / 2, this.height / 2 - 30, 16777215);

		if (!leaving)
		{
			if (nether)
			{
				this.drawCenteredString(this.font, I18n.format("beta.menu.enterNether"), this.width / 2, this.height / 2 - 50, 16777215);
			}
			else
			{
				this.drawCenteredString(this.font, I18n.format("beta.menu.enterEnd"), this.width / 2, this.height / 2 - 50, 16777215);
			}
		}
		else
		{
			if (nether)
			{
				this.drawCenteredString(this.font, I18n.format("beta.menu.leaveNether"), this.width / 2, this.height / 2 - 50, 16777215);
			}
			else
			{
				this.drawCenteredString(this.font, I18n.format("beta.menu.leaveEnd"), this.width / 2, this.height / 2 - 50, 16777215);
			}
		}

		super.render(mouseX, mouseY, partialTicks);
	}

	public boolean isPauseScreen()
	{
		return false;
	}
}